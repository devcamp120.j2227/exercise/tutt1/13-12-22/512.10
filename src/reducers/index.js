// Root reducer (Đây là nơi chứa reducer chính)
import { combineReducers } from "redux"; // Thư viện để tạo reducer

import taskReducer from "./task.reducers";

const rootReducer = combineReducers({
    taskReducer
});

export default rootReducer;