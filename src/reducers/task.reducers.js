import { TASK_INPUT_CHANGE, TASK_ADD_CLICK, TASK_TOGGLE_STATUS } from "../constants/task.constant";

const initialState = {
    input: "",
    taskList: []
}

const taskReducer = (state = initialState, action) => {
    switch (action.type) {
        case TASK_INPUT_CHANGE:
            state.input = action.payload;
            break;
        case TASK_ADD_CLICK:
            state.taskList.push({
                id: state.taskList.length,
                name: state.input,
                status: false
            })
            state.input = "";
            break;
        case TASK_TOGGLE_STATUS:
            state.taskList[action.payload].status = !state.taskList[action.payload].status;
            break;
        default:
            break;
    }
    return { ...state };
}

export default taskReducer;