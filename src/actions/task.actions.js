import { TASK_INPUT_CHANGE, TASK_ADD_CLICK, TASK_TOGGLE_STATUS } from "../constants/task.constant";

// Nơi mô tả những sữ kiện liên quan đến task (Chứa các hàm return về object)
const inputChangeHandler = (value) => {
    return {
        type: TASK_INPUT_CHANGE,
        payload: value
    }
}

const addTaskHandler = () => {
    return {
        type: TASK_ADD_CLICK
    }
}

const taskToggleHandler = (id) => {
    return {
        type: TASK_TOGGLE_STATUS,
        payload: id
    }
}

export { inputChangeHandler, addTaskHandler, taskToggleHandler };